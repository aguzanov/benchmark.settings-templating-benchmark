package com.viaden.crm.core.substitute

import java.io.{ByteArrayOutputStream, OutputStreamWriter, StringReader}

import com.viaden.crm.server.dao.entity.mongo.Settings
import freemarker.template.{Configuration, Template}
import org.bson.types.ObjectId
import org.openjdk.jmh.annotations.{Benchmark, Scope, State}

import scala.collection.JavaConversions._
import Const._

@State(Scope.Benchmark)
class SettingsFreeMarkerSubstitution {

  def f(i: Int): String = s"$${value$i}"

  val s10 = Settings(new ObjectId(), 1, "app_id", "name", Some("Description"), System.currentTimeMillis(), settings(1 to 10, f))
  val s100 = Settings(new ObjectId(), 1, "app_id", "name", Some("Description"), System.currentTimeMillis(), settings(1 to 100, f))
  val s1000 = Settings(new ObjectId(), 1, "app_id", "name", Some("Description"), System.currentTimeMillis(), settings(1 to 1000, f))

  val t10 = template(s10.settings)
  val t100 = template(s100.settings)
  val t1000 = template(s1000.settings)

  val params10 = params(1 to 10)
  val params100 = params(1 to 100)
  val params1000 = params(1 to 1000)

  @Benchmark
  def substitute10(): Unit = {
    apply(s10, t10, params10)
  }

  @Benchmark
  def substitute100(): Unit = {
    apply(s100, t100, params100)
  }

  @Benchmark
  def substitute1000(): Unit = {
    apply(s1000, t1000, params1000)
  }

  def apply(s: Settings, t: Template, p: Map[String, String]): Settings = {
    s.copy(settings = apply(t, p))
  }


  def apply(t: Template, params: java.util.Map[String, String]): String = {
    val baos: ByteArrayOutputStream = new ByteArrayOutputStream()
    val out = new OutputStreamWriter(baos);
    t.process(params, out);
    out.flush()
    baos.toString
  }

  def template(settings: String): Template = {
    new Template("name", new StringReader(settings), new Configuration(Configuration.VERSION_2_3_22))
  }
}
