package com.viaden.crm.core.substitute

import java.io.{ByteArrayOutputStream, OutputStreamWriter, StringReader}

import com.viaden.crm.core.substitute.Const._
import com.viaden.crm.server.dao.entity.mongo.Settings
import org.apache.velocity.app.Velocity
import org.apache.velocity.runtime.RuntimeSingleton
import org.apache.velocity.{Template, VelocityContext}
import org.bson.types.ObjectId
import org.openjdk.jmh.annotations._

import scala.collection.JavaConversions._

@State(Scope.Benchmark)
class SettingsVelocitySubstitution {
  Velocity.init

  val s10 = Settings(new ObjectId(), 1, "app_id", "name", Some("Description"), System.currentTimeMillis(), settings(1 to 10))
  val s100 = Settings(new ObjectId(), 1, "app_id", "name", Some("Description"), System.currentTimeMillis(), settings(1 to 100))
  val s1000 = Settings(new ObjectId(), 1, "app_id", "name", Some("Description"), System.currentTimeMillis(), settings(1 to 1000))

  val t10 = template(s10.settings)
  val t100 = template(s100.settings)
  val t1000 = template(s1000.settings)

  val params10 = params(1 to 10)
  val params100 = params(1 to 100)
  val params1000 = params(1 to 1000)


  @Benchmark
  def substitute10(): Unit = {
    apply(s10, t10, params10)
  }

  @Benchmark
  def substitute100(): Unit = {
    apply(s100, t100, params100)
  }

  @Benchmark
  def substitute1000(): Unit = {
    apply(s1000, t1000, params1000)
  }

  def apply(s: Settings, t: Template, p: Map[String, String]): Settings = {
    s.copy(settings = apply(t, new VelocityContext(p)))
  }

  def apply(t: Template, c: VelocityContext) = {
    val baos: ByteArrayOutputStream = new ByteArrayOutputStream()
    val writer: OutputStreamWriter = new OutputStreamWriter(baos)
    t.merge(c, writer)
    writer.flush()
    baos.toString
  }

  def template(settings: String): Template = {
    val runtimeServices = RuntimeSingleton.getRuntimeServices();
    val reader = new StringReader(settings);
    val node = runtimeServices.parse(reader, "Template name");
    val template = new Template();
    template.setRuntimeServices(runtimeServices);
    template.setData(node);
    template.initDocument();
    template
  }
}
