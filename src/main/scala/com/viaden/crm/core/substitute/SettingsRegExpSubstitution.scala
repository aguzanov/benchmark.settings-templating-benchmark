package com.viaden.crm.core.substitute

import com.viaden.crm.core.substitute.Const._
import com.viaden.crm.server.dao.entity.mongo.Settings
import org.bson.types.ObjectId
import org.openjdk.jmh.annotations._

@State(Scope.Benchmark)
class SettingsRegExpSubstitution {

  val s10 = Settings(new ObjectId(),1,"app_id","name",Some("Description"),System.currentTimeMillis(),settings(1 to 10))
  val s100 = Settings(new ObjectId(),1,"app_id","name",Some("Description"),System.currentTimeMillis(),settings(1 to 100))
  val s1000 = Settings(new ObjectId(),1,"app_id","name",Some("Description"),System.currentTimeMillis(),settings(1 to 1000))

  val params10: Map[String, String] = params(1 to 10)
  val params100: Map[String, String] = params(1 to 100)
  val params1000: Map[String, String] = params(1 to 1000)


  @Benchmark
  def substitute10(): Unit ={
    s10.withInjectedPlaceholders(params10)
  }

  @Benchmark
  def substitute100(): Unit ={
    s100.withInjectedPlaceholders(params100)
  }

  @Benchmark
  def substitute1000(): Unit ={
    s1000.withInjectedPlaceholders(params1000)
  }

}
