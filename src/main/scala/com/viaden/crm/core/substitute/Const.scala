package com.viaden.crm.core.substitute

object Const {
  def params(r: Range, s :(Int) => String = {i => s"value$i"}): Map[String, String] = r.map(i => s(i) -> s"'Value-$i'").toMap
  def settings(r: Range,s :(Int) => String = {i => s"$$value$i"}) : String = "{\n" + r.map(i => s"params$i: ${s(i)}").mkString(",\n") + "\n}"
}
