package com.viaden.crm.core.calculation

import java.io.{ByteArrayOutputStream, OutputStreamWriter, StringReader}

import com.viaden.crm.server.dao.entity.mongo.Settings
import org.apache.velocity.runtime.RuntimeSingleton
import org.apache.velocity.{Template, VelocityContext}
import org.bson.types.ObjectId
import org.openjdk.jmh.annotations.{Scope, State, Benchmark}

import scala.collection.JavaConversions._
import scala.collection.mutable


@State(Scope.Benchmark)
class VelocityElseIfCalculations {
  val purchasecount: String = "purchaseCount"
  val multiplier: String = "multiplier"
  val price: String = "price"

  def params(purchaseCount: Int, price: Int, n: Int): mutable.Map[String, _] = {
    val result: mutable.Map[String,Any] = mutable.Map()
    for (i <- 1 to n) {
      result +=  s"value$i" -> s"'Value-$i'"
    }
    result += this.purchasecount -> purchaseCount
    result += this.price -> price
    result
  }

  def settings(c: Int, p: Int): String = elseif(c, purchasecount, multiplier) + "\n{\n" + (1 to p).map(i => s"params$i: $$value$i").mkString(",\n") + "," +
    "\n#set($price = $multiplier*$price)\nprice : $price\n}"

  def elseif(n: Int, p: String, c: String) = {
    var s: String = s"#if ($$$p<1)\n  #set($$$c = $$$p+1)\n"
    s += (2 to n - 1).map(x => s"#elseif ($$$p<$x)\n  #set($$$c = $$$p+$x)").mkString("\n")
    s += s"\n#else\n  #set($$$c = $$$p+$n)\n#end"
    s
  }


  val s10 = Settings(new ObjectId(), 1, "app_id", "name", Some("Description"), System.currentTimeMillis(), settings(10, 10))
  val s100 = Settings(new ObjectId(), 1, "app_id", "name", Some("Description"), System.currentTimeMillis(), settings(100, 10))
  val s1000 = Settings(new ObjectId(), 1, "app_id", "name", Some("Description"), System.currentTimeMillis(), settings(1000, 10))

  val t10 = template(s10.settings)
  val t100 = template(s100.settings)
  val t1000 = template(s1000.settings)

  val params10 = params(10, 10, 10)
  val params100 = params(100, 10, 10)
  val params1000 = params(1000, 10, 10)


  def apply(s: Settings, t: Template, p: mutable.Map[String, _]): Settings = {
    s.copy(settings = apply(t, new VelocityContext(p)))
  }

  def apply(t: Template, c: VelocityContext) = {
    val baos: ByteArrayOutputStream = new ByteArrayOutputStream()
    val writer: OutputStreamWriter = new OutputStreamWriter(baos)
    t.merge(c, writer)
    writer.flush()
    baos.toString
  }

  def template(settings: String): Template = {
    val runtimeServices = RuntimeSingleton.getRuntimeServices();
    val reader = new StringReader(settings);
    val node = runtimeServices.parse(reader, "Template name");
    val template = new Template();
    template.setRuntimeServices(runtimeServices);
    template.setData(node);
    template.initDocument();
    template
  }



  @Benchmark
  def if_else_10(): Unit = {
    apply(s10, t10, params10)
  }

  @Benchmark
  def if_else_100(): Unit = {
    apply(s100, t100, params100)
  }

  @Benchmark
  def if_else_1000(): Unit = {
    apply(s1000, t1000, params1000)
  }

}
