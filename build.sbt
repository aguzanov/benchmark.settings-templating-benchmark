name := """setting-templating-benchmark"""

version := "1.0-SNAPSHOT"

enablePlugins(JmhPlugin)

libraryDependencies ++= Seq(
   "org.freemarker" % "freemarker" % "2.3.22",
   "org.apache.velocity" % "velocity" %"1.7",
  "org.openjdk.jmh" % "jmh-generator-annprocess" % "1.9.1",
   "com.viaden.offer-server" % "offer-core" % "2.10.4-STG-SNAPSHOT"
)



